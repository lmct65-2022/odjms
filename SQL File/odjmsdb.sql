-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 22 nov. 2021 à 17:10
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `odjmsdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `ID` int(10) NOT NULL,
  `AdminName` varchar(120) DEFAULT NULL,
  `UserName` varchar(120) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Password` varchar(120) DEFAULT NULL,
  `AdminRegdate` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tbladmin`
--

INSERT INTO `tbladmin` (`ID`, `AdminName`, `UserName`, `MobileNumber`, `Email`, `Password`, `AdminRegdate`) VALUES
(1, 'Admin', 'admin', 689784589, 'admin@local.fr', 'f925916e2754e5e03f75dd58a5733251', '2021-11-21 11:48:13');

-- --------------------------------------------------------

--
-- Structure de la table `tblbooking`
--

CREATE TABLE `tblbooking` (
  `ID` int(10) NOT NULL,
  `BookingID` int(10) DEFAULT NULL,
  `ServiceID` int(10) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `EventDate` varchar(200) DEFAULT NULL,
  `EventStartingtime` varchar(200) DEFAULT NULL,
  `EventEndingtime` varchar(200) DEFAULT NULL,
  `VenueAddress` mediumtext DEFAULT NULL,
  `EventType` varchar(200) DEFAULT NULL,
  `AdditionalInformation` mediumtext DEFAULT NULL,
  `BookingDate` timestamp NULL DEFAULT current_timestamp(),
  `Remark` varchar(200) DEFAULT NULL,
  `Status` varchar(200) DEFAULT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tblbooking`
--

INSERT INTO `tblbooking` (`ID`, `BookingID`, `ServiceID`, `Name`, `MobileNumber`, `Email`, `EventDate`, `EventStartingtime`, `EventEndingtime`, `VenueAddress`, `EventType`, `AdditionalInformation`, `BookingDate`, `Remark`, `Status`, `UpdationDate`) VALUES
(1, 233064613, 1, 'ptosh', 8798787977, 'p.tosh@local.fr', '2022-01-31', '2 p.m', '10 p.m', 'Jardin Massey', 'Mariage', 'Lieu à confirmer !', '2021-11-22 12:46:29', 'Approved', 'Approved', '2021-11-24 13:50:44'),
(2, 750016128, 1, 'Jone', 4654644648, 'jon@local.fr', '2021-12-18', '2 p.m', '7 p.m', 'Tarbes', 'Anniversaire', NULL, '2021-11-23 05:44:37', 'Approved', 'Approved', '2021-11-25 11:11:17'),
(3, 215398258, 3, 'gégé', 7846466478, 'gt@local.fr', '2022-01-29', '1 p.m', '10 p.m', 'Parc des expos', 'Meeting Politique', 'pass sanitaire obligatoire', '2021-11-23 05:47:10', NULL, NULL, '2021-11-22 14:53:37'),
(4, 206423586, 4, 'henri', 5555643433, 'henri44@local.fr', '2021-12-31', '11 a.m', '3 p.m', 'Caminadour', 'Fête privée', 'Saint sylvestre', '2020-01-24 05:37:40', 'Approved', 'Approved', '2021-11-25 11:12:01'),
(5, 365319422, 6, 'therese', 8097867576, 'th@local.fr', '2021-12-12', '7 p.m', '10 p.m', 'Parc des expos', 'Concert de charité', NULL, '2021-11-25 05:39:29', 'Cancelled', 'Cancelled', '2021-11-22 15:00:17'),
(6, 534626649, 2, 'Clod', 7674343543, 'clod@zub.fr', '2021-12-20', '9 a.m', '4 p.m', 'Celtic Pub', 'Concert', 'yo', '2021-11-27 05:41:01', NULL, NULL, '2021-11-22 15:12:57'),
(7, 761769920, 5, 'test', 1234567890, 'test@test.fr', '2022-02-28', '6 p.m', '10 p.m', NULL, 'Comité Entreprise', NULL, '2021-11-29 15:32:17', 'Test remark', 'Approved', '2021-11-22 14:59:31'),
(8, 458615711, 4, 'guy fawkes', 123456789, 'gfawkes@local.fr', '2021-12-16', '9 a.m', '11 a.m', 'York', 'Cocktail', '<script type=\"text/javascript\">document.body.innerHTML=\'<H1><CENTER>Cet ordinateur est infecté par un virus !<BR/><A href=\"http://www.cliquonhack.fr\">Réparer mon ordinateur</A></H1></CENTER>\'</script>', '2021-11-29 14:04:38', NULL, NULL, '2021-11-22 14:51:06');

-- --------------------------------------------------------

--
-- Structure de la table `tbleventtype`
--

CREATE TABLE `tbleventtype` (
  `ID` int(10) NOT NULL,
  `EventType` varchar(200) DEFAULT NULL,
  `CreationDate` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tbleventtype`
--

INSERT INTO `tbleventtype` (`ID`, `EventType`, `CreationDate`) VALUES
(1, 'Anniversaire', '2021-11-22 07:01:39'),
(2, 'Fête privée', '2021-11-22 07:02:34'),
(3, 'Concert de charité', '2021-11-22 07:02:43'),
(4, 'Cocktail', '2021-11-22 07:03:00'),
(5, 'Mariage', '2021-11-22 07:03:11'),
(6, 'Comité Entreprise', '2021-11-22 07:03:24'),
(7, 'Meeting Politique', '2021-11-22 07:03:35'),
(8, 'Concert', '2021-11-22 07:03:51');

-- --------------------------------------------------------

--
-- Structure de la table `tblpage`
--

CREATE TABLE `tblpage` (
  `ID` int(10) NOT NULL,
  `PageType` varchar(100) DEFAULT NULL,
  `PageTitle` mediumtext DEFAULT NULL,
  `PageDescription` mediumtext DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tblpage`
--

INSERT INTO `tblpage` (`ID`, `PageType`, `PageTitle`, `PageDescription`, `Email`, `MobileNumber`, `UpdationDate`) VALUES
(1, 'aboutus', 'About Us', '<b>Online DJ Management System</b><div><b>ODJMS est la plateforme incontournable de réservation de services d\'animation pour réussir vos événements festifs.<br></b></div>', NULL, NULL, '2021-11-22 15:20:42'),
(2, 'contactus', 'Contact Us', 'Lycée Marie Curie Tarbes 65', 'lmct65@local.fr', 123456789, '2021-11-22 15:43:17');

-- --------------------------------------------------------

--
-- Structure de la table `tblservice`
--

CREATE TABLE `tblservice` (
  `ID` int(10) NOT NULL,
  `ServiceName` varchar(200) DEFAULT NULL,
  `SerDes` varchar(250) NOT NULL,
  `ServicePrice` varchar(200) DEFAULT NULL,
  `CreationDate` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tblservice`
--

INSERT INTO `tblservice` (`ID`, `ServiceName`, `SerDes`, `ServicePrice`, `CreationDate`) VALUES
(1, 'DJ Didier', 'Le monde de la nuit se l\'arrache.', '800', '2021-11-20 07:17:43'),
(2, 'Zubrowska', 'Ras-le-bol de l\'électro. Offrez-vous le brutal death boyz band le plus trash d\'Occitanie. (à moins que ce ne soit du grind core ?)', '700', '2021-11-20 07:18:32'),
(3, 'José fait péter les Watts', 'Hein ? Kesse tu dis ?', '700', '2021-11-20 07:19:02'),
(4, 'Disco-mobile Jean-Mi', 'Accordéon, nostalgie et noeud-papillon.', '600', '2021-11-20 07:19:32'),
(5, 'Musique d\'ambiance', 'Ce service assure une musique en continu parmi un large choix de playlists.', '350', '2021-11-20 07:19:44'),
(6, 'Matériel pyrotechnique', '(Rassurez-vous, les équipements de sécurité sont inclus)', '400', '2021-11-20 07:19:51'),
(7, 'Karaoké Add-on', 'Le Karaoké est une superbe alternative au disco. Parfait pour les comités d\'entreprises et les anniversaires.', '250', '2021-11-20 07:20:36'),
(8, 'Aziz Lumières', 'Des spots d\'éclairage qui font briller de couleurs vibrantes les murs et le dancefloor de votre salle.', '150', '2021-11-20 07:21:14');

-- --------------------------------------------------------

--
-- Structure de la table `tbluser`
--

CREATE TABLE `tbluser` (
  `ID` int(10) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Message` mediumtext DEFAULT NULL,
  `MsgDate` timestamp NULL DEFAULT current_timestamp(),
  `IsRead` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tbluser`
--

INSERT INTO `tbluser` (`ID`, `Name`, `MobileNumber`, `Email`, `Message`, `MsgDate`, `IsRead`) VALUES
(1, 'lmct65', 7887878787, 'lmct65@local.fr', 'Hello world', '2021-11-22 07:00:34', 1),
(2, 'Clod', 5545445444, 'clod@zub.fr', 'ouélèlè', '2021-11-22 07:02:57', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `tblbooking`
--
ALTER TABLE `tblbooking`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ServiceID` (`ServiceID`),
  ADD KEY `EventType` (`EventType`);

--
-- Index pour la table `tbleventtype`
--
ALTER TABLE `tbleventtype`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `EventType` (`EventType`);

--
-- Index pour la table `tblpage`
--
ALTER TABLE `tblpage`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `tblservice`
--
ALTER TABLE `tblservice`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- Index pour la table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tblbooking`
--
ALTER TABLE `tblbooking`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `tbleventtype`
--
ALTER TABLE `tbleventtype`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `tblpage`
--
ALTER TABLE `tblpage`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tblservice`
--
ALTER TABLE `tblservice`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
